package co.com.demoblaze.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Register {
    public static final Target SIGN_IN = Target.the("Sign in").
            located(By.id("signin2"));
    public static final Target SIGN_IN_USER_NAME = Target.the("Sign in user name").
            located(By.id("sign-username"));
    public static final Target SIGN_IN_PASSWORD = Target.the("Sign in password").
            located(By.id("sign-password"));
    public static final Target BUTTON_SIGN_UP = Target.the("Sign in password").
            located(By.xpath("//button[contains(text(),'Sign up')]"));
}
