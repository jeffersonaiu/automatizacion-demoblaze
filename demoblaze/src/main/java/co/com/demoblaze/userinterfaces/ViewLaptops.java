package co.com.demoblaze.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ViewLaptops {
    public static final Target LAPTOPS = Target.the("Laptops").
            located(By.xpath("//a[contains(text(),'Laptops')]"));
    public static final Target FIRST_LAPTOPS_MACBOOK_AIR = Target.the("First laptops").
            located(By.xpath("(//a[@class='hrefch'])[3]"));
    public static final Target NAME_FIRST_LAPTOPS = Target.the("Name laptops").
            located(By.xpath("//h2[contains(text(),'MacBook air')]"));
    public static final Target PRICE_FIRST_LAPTOPS = Target.the("Price laptops").
            located(By.xpath("//h3[contains(text(),'$700')]"));
    public static final Target DESCRIPTIONS_FIRST_LAPTOPS = Target.the("Descriptions laptops").
            located(By.xpath("//strong[contains(text(),'Product description')]"));

    public static final Target SECOND_LAPTOPS = Target.the("Second laptops").
            located(By.xpath("(//a[@class='hrefch'])[5]"));
    public static final Target NAME_SECOND_LAPTOPS = Target.the("Name laptops").
            located(By.xpath("//h2[contains(text(),'2017 Dell 15.6 Inch')]"));
    public static final Target IMG = Target.the("Img").
            located(By.xpath("//img[@width='400']"));

}
