package co.com.demoblaze.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LogInPage {
    public static final Target LOGIN = Target.the("Log In").
            located(By.id("login2"));
    public static final Target LOGIN_USERNAME = Target.the("Log In username").
            located(By.id("loginusername"));
    public static final Target LOGIN_PASSWORD = Target.the("Log In pasword").
            located(By.id("loginpassword"));
    public static final Target BUTTON_LOGIN = Target.the("Button Log In").
            located(By.xpath("//button[contains(text(),'Log in')]"));
    public static final Target WELCOME = Target.the("Welcome").
            located(By.id("nameofuser"));
}
