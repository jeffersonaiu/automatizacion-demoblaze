package co.com.demoblaze.questions.laptops;

import co.com.demoblaze.userinterfaces.ViewLaptops;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class VerifyNameFirstLaptops implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        return ViewLaptops.NAME_FIRST_LAPTOPS.resolveFor(actor).getText().equals("MacBook air");
    }
    public static Question <Boolean> verifyFirst() {
        return new VerifyNameFirstLaptops();
    }

}
