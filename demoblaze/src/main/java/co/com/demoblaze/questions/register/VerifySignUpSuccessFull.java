package co.com.demoblaze.questions.register;

import co.com.demoblaze.userinterfaces.LogInPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class VerifySignUpSuccessFull implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        return LogInPage.LOGIN.resolveFor(actor).getTextContent().contains("Log in");
    }
    public static Question <Boolean> verifyLoginSingle() {
        return new VerifySignUpSuccessFull();
    }

}
