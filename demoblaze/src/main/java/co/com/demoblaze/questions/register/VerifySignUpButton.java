package co.com.demoblaze.questions.register;

import co.com.demoblaze.userinterfaces.LogInPage;
import co.com.demoblaze.userinterfaces.Register;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class VerifySignUpButton implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        return Register.BUTTON_SIGN_UP.resolveFor(actor).getText().equals("Sign up");
    }

    public static Question<Boolean> verifySignUpButton() {
        return new VerifySignUpButton();
    }

}
