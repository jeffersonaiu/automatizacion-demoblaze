package co.com.demoblaze.questions.laptops;

import co.com.demoblaze.userinterfaces.ViewLaptops;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class VerifyNameSecondLaptops implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        return ViewLaptops.NAME_SECOND_LAPTOPS.resolveFor(actor).getText().equals("2017 Dell 15.6 Inch");
    }
    public static Question <Boolean> verifyFirst() {
        return new VerifyNameSecondLaptops();
    }

}
