package co.com.demoblaze.questions.login;

import co.com.demoblaze.userinterfaces.LogInPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class VerifyLoginButton implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        return LogInPage.BUTTON_LOGIN.resolveFor(actor).getText().equals("Log in");
    }
    public static Question <Boolean> verifyLoginButton() {
        return new VerifyLoginButton();
    }

}
