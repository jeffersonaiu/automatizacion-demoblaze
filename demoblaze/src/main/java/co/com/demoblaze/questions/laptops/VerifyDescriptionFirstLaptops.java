package co.com.demoblaze.questions.laptops;

import co.com.demoblaze.userinterfaces.ViewLaptops;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class VerifyDescriptionFirstLaptops implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        return ViewLaptops.DESCRIPTIONS_FIRST_LAPTOPS.resolveFor(actor).getText().equals("Product description");
    }
    public static Question <Boolean> verifyFirstDescriptions() {
        return new VerifyDescriptionFirstLaptops();
    }

}
