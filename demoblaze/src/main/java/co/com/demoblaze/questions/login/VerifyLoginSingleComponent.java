package co.com.demoblaze.questions.login;

import co.com.demoblaze.userinterfaces.LogInPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class VerifyLoginSingleComponent implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        return LogInPage.WELCOME.resolveFor(actor).getTextContent().contains("");
    }
    public static Question <Boolean> verifySuccessFull() {
        return new VerifyLoginSingleComponent();
    }

}
