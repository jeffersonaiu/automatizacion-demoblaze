package co.com.demoblaze.questions.laptops;

import co.com.demoblaze.userinterfaces.ViewLaptops;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class VerifyPriceFirstLaptops implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        return ViewLaptops.PRICE_FIRST_LAPTOPS.resolveFor(actor).getTextContent().contains("$700");
    }
    public static Question <Boolean> verifyFirstPrice() {
        return new VerifyPriceFirstLaptops();
    }

}
