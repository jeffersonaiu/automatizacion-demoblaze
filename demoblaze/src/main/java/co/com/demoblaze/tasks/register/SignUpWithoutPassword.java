package co.com.demoblaze.tasks.register;

import co.com.demoblaze.interactions.Wait;
import co.com.demoblaze.utils.AceptarVentanaEmergente;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static co.com.demoblaze.userinterfaces.Register.*;
import static co.com.demoblaze.utils.UtilRegister.USER;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SignUpWithoutPassword implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Click.on(SIGN_IN),
                Enter.theValue(USER).into(SIGN_IN_USER_NAME),
                Click.on(BUTTON_SIGN_UP),
                Wait.theSeconds(3),
                AceptarVentanaEmergente.aceptarVentanaEmergente(),
                Wait.theSeconds(5)

        );

    }

    public static SignUpWithoutPassword signUpWithoutPassword() {
        return instrumented(SignUpWithoutPassword.class);
    }
}
