package co.com.demoblaze.tasks.login;

import co.com.demoblaze.interactions.Wait;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static co.com.demoblaze.userinterfaces.LogInPage.*;
import static co.com.demoblaze.userinterfaces.LogInPage.BUTTON_LOGIN;
import static co.com.demoblaze.utils.UtilRegister.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LogInSingleComponent implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(LOGIN),
                Enter.theValue(USER_SINGLE_COMPONENT).into(LOGIN_USERNAME),
                Enter.theValue(PASSWORD_SINGLE_COMPONENT).into(LOGIN_PASSWORD),
                Click.on(BUTTON_LOGIN),
                Wait.theSeconds(3)


        );
    }
    public static LogInSingleComponent logInSingleComponente(){
        return instrumented(LogInSingleComponent.class);}
}
