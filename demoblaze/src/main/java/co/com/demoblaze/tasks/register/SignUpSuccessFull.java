package co.com.demoblaze.tasks.register;

import co.com.demoblaze.interactions.Wait;
import co.com.demoblaze.utils.AceptarVentanaEmergente;
import co.com.demoblaze.utils.SaveData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.Date;

import static co.com.demoblaze.userinterfaces.Register.*;
import static co.com.demoblaze.utils.SaveDataPathFile.PATH_FILE;
import static co.com.demoblaze.utils.UtilRegister.PASSWORD;
import static co.com.demoblaze.utils.UtilRegister.USER;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SignUpSuccessFull implements Task {
    String code = Long.toString(new Date().getTime()).substring(4,10);
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(SIGN_IN),
                Enter.theValue(USER+code).into(SIGN_IN_USER_NAME),
                Enter.theValue(PASSWORD).into(SIGN_IN_PASSWORD),
                Click.on(BUTTON_SIGN_UP),
                Wait.theSeconds(3),
                AceptarVentanaEmergente.aceptarVentanaEmergente(),
                Wait.theSeconds(5)

        );
        SaveData.getInstance(PATH_FILE).writeFile(USER + code);
        actor.remember("code", code);
    }
    public static SignUpSuccessFull signUpSuccessFull() {
        return instrumented(SignUpSuccessFull.class);}
}
