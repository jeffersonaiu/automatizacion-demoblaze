package co.com.demoblaze.tasks.login;

import co.com.demoblaze.interactions.Wait;
import co.com.demoblaze.tasks.register.SignUpExist;
import co.com.demoblaze.utils.AceptarVentanaEmergente;
import co.com.demoblaze.utils.SaveData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static co.com.demoblaze.userinterfaces.LogInPage.*;
import static co.com.demoblaze.utils.SaveDataPathFile.PATH_FILE;
import static co.com.demoblaze.utils.UtilRegister.USER;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LogInWithoutPassword implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(LOGIN),
                Enter.theValue(USER).into(LOGIN_USERNAME),
                Click.on(BUTTON_LOGIN),
                Wait.theSeconds(3),
                AceptarVentanaEmergente.aceptarVentanaEmergente(),
                Wait.theSeconds(3)

        );
    }
    public static LogInWithoutPassword logInWithoutPassword(){
        return instrumented(LogInWithoutPassword.class);
    }
}
