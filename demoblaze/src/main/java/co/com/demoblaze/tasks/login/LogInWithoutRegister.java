package co.com.demoblaze.tasks.login;

import co.com.demoblaze.interactions.Wait;
import co.com.demoblaze.utils.AceptarVentanaEmergente;
import co.com.demoblaze.utils.SaveData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.Date;

import static co.com.demoblaze.userinterfaces.LogInPage.*;
import static co.com.demoblaze.userinterfaces.Register.SIGN_IN_USER_NAME;
import static co.com.demoblaze.utils.SaveDataPathFile.PATH_FILE;
import static co.com.demoblaze.utils.UtilRegister.PASSWORD;
import static co.com.demoblaze.utils.UtilRegister.USER;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LogInWithoutRegister implements Task {

    String code = Long.toString(new Date().getTime()).substring(6,10);
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(LOGIN),
                Enter.theValue(USER+code).into(LOGIN_USERNAME),
                Enter.theValue(PASSWORD).into(LOGIN_PASSWORD),
                Click.on(BUTTON_LOGIN),
                Wait.theSeconds(3),
                AceptarVentanaEmergente.aceptarVentanaEmergente(),
                Wait.theSeconds(3)


        );
    }
    public static LogInWithoutRegister logInWithoutRegister(){
        return instrumented(LogInWithoutRegister.class);}
}
