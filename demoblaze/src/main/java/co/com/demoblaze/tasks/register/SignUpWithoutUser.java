package co.com.demoblaze.tasks.register;

import co.com.demoblaze.interactions.Wait;
import co.com.demoblaze.utils.AceptarVentanaEmergente;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static co.com.demoblaze.userinterfaces.Register.*;
import static co.com.demoblaze.utils.UtilRegister.PASSWORD;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SignUpWithoutUser implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Click.on(SIGN_IN),
                Enter.theValue(PASSWORD).into(SIGN_IN_PASSWORD),
                Click.on(BUTTON_SIGN_UP),
                Wait.theSeconds(3),
                AceptarVentanaEmergente.aceptarVentanaEmergente(),
                Wait.theSeconds(5)

        );

        }
    public static SignUpWithoutUser signUpWithoutUser(){

        return instrumented(SignUpWithoutUser.class);
    }
    }

