package co.com.demoblaze.stepdefenitions;

import co.com.demoblaze.questions.login.VerifyLoginButton;
import co.com.demoblaze.questions.login.VerifyLoginSingleComponent;
import co.com.demoblaze.tasks.login.LogInSingleComponent;
import co.com.demoblaze.tasks.login.LogInWithoutPassword;
import co.com.demoblaze.tasks.login.LogInWithoutRegister;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class LogIn {



    @When("^Enter the log in register without password$")
    public void enterTheLogInRegisterWithoutPassword() {
        theActorInTheSpotlight().attemptsTo(LogInWithoutPassword.logInWithoutPassword());
    }

    @Then("^log in demoblaze fail$")
    public void logInDemoblazeFail() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyLoginButton.verifyLoginButton()));
    }

    @When("^Enter the page without register$")
    public void enterThePageWithoutRegister() {
        theActorInTheSpotlight().attemptsTo(LogInWithoutRegister.logInWithoutRegister());
    }

    @Then("^Log In Without Register fail$")
    public void logInWithoutRegisterFail() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyLoginButton.verifyLoginButton()));
    }
    @When("^Enter the page and register with single component$")
    public void enterThePageAndRegisterWithSingleComponent() {
        theActorInTheSpotlight().attemptsTo(LogInSingleComponent.logInSingleComponente());
    }

    @Then("^Log In single component fail$")
    public void logInSingleComponentFail() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyLoginSingleComponent.verifySuccessFull()));
    }


}
