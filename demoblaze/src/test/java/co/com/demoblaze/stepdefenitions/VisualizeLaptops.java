package co.com.demoblaze.stepdefenitions;

import co.com.demoblaze.questions.laptops.*;
import co.com.demoblaze.tasks.visualizelaptops.ViewFirtLaptops;
import co.com.demoblaze.tasks.visualizelaptops.ViewSecondLaptops;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class VisualizeLaptops {
    @When("^Enter the component laptops and view first laptops$")
    public void enterTheComponentLaptopsAndViewFirstLaptops() {
        theActorInTheSpotlight().attemptsTo(ViewFirtLaptops.viewFirtLaptops());

    }

    @Then("^view product name first laptops$")
    public void viewProductNameFirstLaptops() {
       theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyNameFirstLaptops.verifyFirst()));
    }

    @Then("^view price first laptops$")
    public void viewPriceFirstLaptops() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyPriceFirstLaptops.verifyFirstPrice()));
    }

    @Then("^View description first laptops$")
    public void viewDescriptionFirstLaptops() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyDescriptionFirstLaptops.verifyFirstDescriptions()));
    }

    @When("^Enter the component laptops and view second laptops$")
    public void enterTheComponentLaptopsAndViewSecondLaptops() {
        theActorInTheSpotlight().attemptsTo(ViewSecondLaptops.viewSecondLaptops());
    }

    @Then("^view product name second laptops$")
    public void viewProductNameSecondLaptops() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyNameSecondLaptops.verifyFirst()));
    }

    @Then("^view price second laptops$")
    public void viewPriceSecondLaptops() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyPriceFirstLaptops.verifyFirstPrice()));
    }

    @Then("^View description second laptops$")
    public void viewDescriptionSecondLaptops() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyDescriptionFirstLaptops.verifyFirstDescriptions()));
    }


}
