package co.com.demoblaze.stepdefenitions;

import co.com.demoblaze.questions.login.VerifyLoginButton;
import co.com.demoblaze.questions.login.VerifyLoginSingleComponent;
import co.com.demoblaze.questions.register.VerifySignUpButton;
import co.com.demoblaze.questions.register.VerifySignUpSuccessFull;
import co.com.demoblaze.tasks.register.SignUpExist;
import co.com.demoblaze.tasks.register.SignUpSuccessFull;
import co.com.demoblaze.tasks.register.SignUpWithoutPassword;
import co.com.demoblaze.tasks.register.SignUpWithoutUser;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class Register {
    @Before
    public void setThestago() {
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled("yeferson");
    }

    @Given("^into in the page demoblaze$")
    public void intoInThePageDemoblaze() {
        theActorInTheSpotlight().wasAbleTo(Open.url("https://www.demoblaze.com/index.html"));
    }


    @When("^Enter the sign in and register without password$")
    public void enterTheSignInAndRegisterWithoutPassword() {
        theActorInTheSpotlight().attemptsTo(SignUpWithoutPassword.signUpWithoutPassword());
    }

    @Then("^Register without password fail$")
    public void registerWithoutPasswordFail() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifySignUpButton.verifySignUpButton()));
    }

    @When("^Enter the sign in and register without user$")
    public void enterTheSignInAndRegisterWithoutUser() {
        theActorInTheSpotlight().attemptsTo(SignUpWithoutUser.signUpWithoutUser());
    }

    @Then("^Register without user fail$")
    public void registerWithoutUserFail() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifySignUpButton.verifySignUpButton()));
    }

    @When("^Enter the sign in and register success full$")
    public void enterTheSignInAndRegisterSuccessFull() {
        theActorInTheSpotlight().attemptsTo(SignUpSuccessFull.signUpSuccessFull());
    }

    @Then("^Register success full$")
    public void registerSuccessFull() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifySignUpSuccessFull.verifyLoginSingle()));
    }

    @When("^Enter the sign in and register Exist\\.$")
    public void enterTheSignInAndRegisterExist() {
        theActorInTheSpotlight().attemptsTo(SignUpExist.signUpExist());
    }

    @Then("^Register Exist$")
    public void registerExist() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifySignUpButton.verifySignUpButton()));
    }

}
