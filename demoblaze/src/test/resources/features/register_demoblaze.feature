@stories
Feature: I as a user want to make a registration
  on the demoblaze page

  Background:

    Given  into in the page demoblaze

  @signUpWithoutPassword
  Scenario: Register in demoblaze without password
    When Enter the sign in and register without password
    Then Register without password fail

  @signUpWithoutUser
  Scenario: Register in demoblaze without user
    When Enter the sign in and register without user
    Then Register without user fail

  @signUpSuccessFull
  Scenario: Register in demoblaze success full
    When Enter the sign in and register success full
    Then Register success full

  @signUpExist.
  Scenario: Register in demoblaze Exist.
    When Enter the sign in and register Exist.
    Then Register Exist


